//
//  node.hpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 10/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#ifndef node_hpp
#define node_hpp

#include <stdio.h>
#include <string>

using namespace std;

class Node
{
    public:
    Node(string _name, string _value);
    string name;
    string value;
    
    
};

#endif /* node_hpp */
