//
//  arg_parser.cpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 11/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#include "arg_parser.hpp"

void ap_getRequestType(int argc, string argv[], struct reqs *_req_struct)
{
    if(argc <= 1 || (argc == 2 && argv[1] == "-h") || argc > 7)
    {
        ap_showHelp();
        _req_struct->type = HELP;
        _req_struct->error = true;
        return;
    }
    else if(argc == 2 && argv[1] == "-l")
    {
        _req_struct->type = LIST_VAULTS;
        _req_struct->error = false;
    }
    // possible variants CREATE_VAULT |
    else if(argc == 3)
    {
        if(argv[1] == "-cv")
        {
            _req_struct->type = CREATE_VAULT;
            _req_struct->vaultName = argv[2];
            _req_struct->error = false;
            return;
        }
        else if(argv[1] == "-rm")
        {
            _req_struct->type = REMOVE_VAULT;
            _req_struct->vaultName = argv[2];
            _req_struct->error = false;
        }
        else
        {
            ap_showHelp();
            _req_struct->error = true;
            return;
        }
        
    }
    
    // possible variants GET_ALL_NODES |
    else if(argc == 4)
    {
        if(argv[1] == "-all" && argv[2] == "-v")
        {
            _req_struct->type = GET_ALL_NODES;
            _req_struct->vaultName = argv[3];
            _req_struct->error = false;
        }
        else
        {
            ap_showHelp();
            _req_struct->error = true;
            return;
        }
    }
    
    //GET_VALUE, // -g <node name> -v <vault name>
    //REMOVE_NODE, // -rm <node name> -v <vault name>
    // ADD_NODE, // -a <node_name> -v <vault name>
    // possible variants GET_VALUE | REMOVE_NODE
    else if(argc == 5)
    {
        if(argv[1] == "-g" && argv[3] == "-v")
        {
            _req_struct->type = GET_VALUE;
            _req_struct->nodeName = argv[2];
            _req_struct->vaultName = argv[4];
            _req_struct->error = false;
            return;
        }
        else if(argv[1] == "-rm" && argv[3] == "-v")
        {
            _req_struct->type = REMOVE_NODE;
            _req_struct->nodeName = argv[2];
            _req_struct->vaultName = argv[4];
            _req_struct->error = false;
            return;
        }
        else if(argv[1] == "-a" && argv[3] == "-v")
        {
            _req_struct->type = ADD_NODE;
            _req_struct->nodeName = argv[2];
            _req_struct->vaultName = argv[4];
            
            cout << "Enter password for " << _req_struct->nodeName << ":";
            getline(cin, _req_struct->value);
            if(_req_struct->value.length() == 0)
            {
                _req_struct->error = true;
                cout << "Password cannot be empty" << endl;
                return;
            }
            
            for(int i=0; i< _req_struct->value.length(); i++)
            {
                if(_req_struct->value[i] == ' ')
                {
                    _req_struct->error = true;
                    cout << "Password cannot contain spaces" << endl;
                    return;
                }
            }
            _req_struct->error = false;
        }
        else
        {
            ap_showHelp();
            _req_struct->error = true;
            return;
        }
    }
    // ADD NODE, // -a <node name> -v <vault name> [-gen [#]]
    else if(argc == 6 || argc == 7)
    {
        if(argv[1] == "-a" && argv[3] == "-v" && argv[5] == "-gen")
        {
            _req_struct->type = ADD_NODE;
            _req_struct->nodeName = argv[2];
            _req_struct->vaultName = argv[4];
            _req_struct->generate = true;
            _req_struct->error = false;
            if(argc == 6) //-a <node name> -v <vault name> -gen
            {
                _req_struct->genLength = -1;
            }
            else if(argc == 7) // -a <node name> -v <vault name> -gen #
            {
                string numChars = string(argv[6]);
                for(int i=0; i < numChars.length(); i++)
                {
                    if(!isdigit(numChars[i]))
                    {
                        cout << "Password length should be a number" << endl;
                        ap_showHelp();
                        _req_struct->error = true;
                        return;
                    }
                }
                
                _req_struct->genLength = stoi(argv[6]);
                if(_req_struct->genLength < 8 || _req_struct->genLength > 49)
                {
                    cout << "Password length should be between 8 and 50 characters long" << endl;
                    ap_showHelp();
                    _req_struct->error = true;
                    return;
                }
            }
        }
    }
    
    else // unreachable -> just in case
    {
        ap_showHelp();
        _req_struct->error = true;
        return;
    }
}

void ap_showHelp()
{
    char blue[] = { 0x1b, '[', '1', ';', '3', '4', 'm', 0 };
    char red_back[] = {0x1b, '[', '1', ';', '4', '1', 'm', 0};
    char normal[] = { 0x1b, '[', '0', ';', '3', '9', 'm', 0 };
    
    
    cout << blue << "Thanks for using atlaskeepr," << normal << endl;
    cout << red_back << "Please be aware that if you lose your vault password - data will be lost forever" << normal << endl;
    cout << "Usage: " << endl;
    cout << "-l" << " to list all vaults" << endl;
    cout << "-cv <vault name>" << " to create vault" << endl;
    cout << "-rm <vault name>" << " to remove vault" << endl;
    cout << "-a <node name> -v <vault name> [-gen [#]] to add node, where # - number of symbols to generate (8, 50)" << endl;
    cout << "-g <node name> -v <vault name>" << " to get associated value" << endl;
    cout << "-rm <node name> -v <vault name>" << " to remove node" << endl;
    cout << "-h" << " show this help file" << endl;
    cout << "-all -v <vault name>" << " to get all nodes from vault" << endl;
    
}


