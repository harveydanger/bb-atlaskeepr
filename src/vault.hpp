//
//  vault.hpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 10/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#ifndef vault_hpp
#define vault_hpp

#include <string.h>
#include <vector>
#include <iostream>
#include "node.hpp"
#include "macro.h"

using namespace std;

class Vault
{
private:

    string name;
    vector<Node> nodes;
    int addNode(Node *_node);
    
public:
    
    void setVaultPath(string _name);
    Vault(string _path);
    
    int createNode(string _name, string _value);
    void generateNode(string _dirty);
    
    string getNodeValueByName(string _name);
    vector<string> getAllNodes();
    string getAllNodesPlain();
    void printAllNodes();
    
    int removeNode(string _name);

};

#endif /* vault_hpp */
