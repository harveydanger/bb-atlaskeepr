//
//  controller.cpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 10/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//
#include "controller.hpp"

struct reqs *Controller::getRequest()
{
    return &this->request;
}

Controller::Controller()
{
    this->fs = new FSWriter();
    this->homeDir = this->getCurrentHomeFolder() + "/" + DEF_DIR;
    
    if(!this->fs->dirExists(this->homeDir))
    {
        cout << "Default storage does not exist, creating..." << endl;
        if(this->fs->createDefaultFolder(this->homeDir) != OK_EXIT_CODE)
        {
            cout << " Could not create default atlas storage" << endl;
        }
    }
}

string *Controller::getStringArguments(int argc, const char *args[])
{
    string *s = new string[argc];
    char i;
    
    for(i=0; i<argc; i++)
    {
        s[i] = args[i];
    }
    
    return s;
}

string Controller::getCurrentHomeFolder()
{
    struct passwd *pw = getpwuid(getuid());
    string dir(pw->pw_dir);
    
    return dir;
}

string Controller::getFullPath(string vaultName)
{
    string s = this->homeDir + "/" + vaultName;
    
    return s;
}

//CREATE_VAULT, // -cv <vault name>
//ADD_NODE, // -a <node name> -v <vault name>
//GET_VALUE, // -g <node name> -v <vault name>
//REMOVE_NODE, // -rm <node name> -v <vault name>
//HELP, // -h
//GET_ALL_NODES // -all -v <vault name>

int Controller::run( int argc, const char *argv[] )
{
    // 1. parse arguments
    this->delegateArguments(argc, argv);
    
    // 2. decide on further actions
    if(this->getRequest()->error)
    {
        return ERROR_EXIT_CODE;
    }
    
    // 3. Load vault
    this->vault = new Vault(this->getRequest()->vaultName);
    
    if(this->getRequest()->type != HELP && this->getRequest()->type != LIST_VAULTS
       && this->getRequest()->type != REMOVE_VAULT)
    {
        this->password =this->retrievePassword();
        this->strToIntKey(password, this->key, PASS_LEN);
        
        if(this->getRequest()->type != CREATE_VAULT)
        {
            vector<uint8_t> read = this->fs->readVault_uint(this->getFullPath(this->getRequest()->vaultName));
            vector<string> items = this->getVaultRows(this->decrypt_uint(read));
            
            for(auto&& item: items)
            {
                this->vault->generateNode(item);
            }
            
            if(this->vault->getAllNodes().empty() && this->getRequest()->type != CREATE_VAULT)
            {
                cout << "Wrong vault password supplied" << endl;
                return ERROR_EXIT_CODE;
            }
        }
    }
   
    // 4. perform an action
    switch(this->getRequest()->type)
    {
        case CREATE_VAULT:
            if(this->fs->createVault(this->getFullPath(this->getRequest()->vaultName)) == OK_EXIT_CODE)
            {
                uint8_t *data = this->encrypt_uint(ATLAS_PREFIX);
                this->fs->writeVault_uint(data, this->dataLen, this->getFullPath(this->getRequest()->vaultName));
                cout << "Vault " << this->getRequest()->vaultName << " has been created." << endl;
            }
            else
            {
                cout << "Vault " << this->getRequest()->vaultName << " already exists." << endl;
            }
            break;
            
        case REMOVE_VAULT:
            if(this->fs->removeVault(this->getFullPath(this->getRequest()->vaultName)) == OK_EXIT_CODE)
            {
                cout << "Vault " << this->getRequest()->vaultName << " has been removed." << endl;
            }
            else
            {
                cout << "Vault " << this->getRequest()->vaultName << " does not exist." << endl;
            }
            break;
            
        case ADD_NODE:
            
            if(this->getRequest()->generate)
            {
                if(this->getRequest()->genLength != -1)
                {
                    this->getRequest()->value = this->generatePassword(this->getRequest()->genLength);
                }
                else
                {
                    this->getRequest()->value = this->generatePassword(DEFAULT_PASS_LEN);
                }
            }
            
            if(this->vault->createNode(this->getRequest()->nodeName, this->getRequest()->value) == OK_EXIT_CODE)
            {
                string nodes = this->vault->getAllNodesPlain();
                uint8_t *data = this->encrypt_uint(nodes);
                this->fs->writeVault_uint(data, this->dataLen, this->getFullPath(this->getRequest()->vaultName));
                cout << "Node " << this->getRequest()->nodeName << (this->getRequest()->generate ? ":" + this->getRequest()->value : "") << " has been added to " << this->getRequest()->vaultName << endl;
            }
            else
            {
                cout << "Node " << this->getRequest()->nodeName << " already exists." << endl;
            }
            break;
            
        case GET_VALUE:
            cout << this->vault->getNodeValueByName(this->getRequest()->nodeName) << endl;
            break;
            
        case REMOVE_NODE:
            if(this->vault->removeNode(this->getRequest()->nodeName) == OK_EXIT_CODE)
            {
                string nodes = this->vault->getAllNodesPlain();
                uint8_t *data = this->encrypt_uint(nodes);
                this->fs->writeVault_uint(data, this->dataLen, this->getFullPath(this->getRequest()->vaultName));
            }
            else
            {
                cout << "Node " << this->getRequest()->nodeName << " does not exist." << endl;
            }
            break;
            
        case GET_ALL_NODES:
            this->vault->printAllNodes();
            break;
        
        case LIST_VAULTS:
            this->fs->listAllVaults(this->getCurrentHomeFolder() + "/" + DEF_DIR);
            break;
            
        case HELP:
        default:
            break;
    }
    return OK_EXIT_CODE;
}


string Controller::retrievePassword()
{
    string password;
    cout << "Enter vault password:" << endl;
    cin >> password;
    
    return password;
}

void Controller::delegateArguments(int argc, const char *argv[])
{
    string *s = this->getStringArguments(argc, argv);
    ap_getRequestType(argc, s, &this->request);
}

void Controller::strToIntKey(string key, uint8_t *store, int keyLen)
{
    for(int i = 0; i < keyLen; i++)
    {
        if(i >= key.length())
        {
            store[i] = 0x00;
        }
        else
        {
            store[i] = key[i];
        }
    }
}

uint8_t *Controller::encrypt_uint(string row)
{
    int steps = 1;
    
    while(steps * CHUNK < row.length())
    {
        ++steps;
    }
    
    this->dataLen = steps * CHUNK;
    uint8_t *result = new uint8_t[steps*CHUNK];
 
    for(int out = 0 ; out < steps ; out++)
    {
        uint8_t buffer[CHUNK];
        memset(&buffer, 0x00, CHUNK);
        
        for(int in = 0; in < CHUNK; in++)
        {
            if(row.length() <= in + (out * CHUNK))
            {
                buffer[in] = 0x00;
            }
            
            else
            {
                buffer[in] = row[in + (out * CHUNK) ];
            }
        }
        
        AES_init_ctx_iv(&this->ctx, this->key, this->iv);
        AES_CBC_encrypt_buffer(&this->ctx, buffer, CHUNK);
        
        this->mutateArray(buffer, result, CHUNK, steps * CHUNK, out * CHUNK);
    }
    
    return result;
}


string Controller::decrypt_uint(vector<uint8_t> data)
{
    
    int steps = (int)data.size() / CHUNK;
    string result = "";
    
    /* compulsory for data swap, as string += operation
     inserts redundant symbols even with the initialization*/
    char tChArr[(int)data.size()];
    
    memset(&tChArr, 0x00, data.size());
    for(int out = 0; out < steps; out++)
    {
        uint8_t buffer[CHUNK];
        memset(&buffer, 0x00, CHUNK);
        
        for(int in = 0; in < CHUNK; in++)
        {
            buffer[in] = data.at(in + (out * CHUNK));
        }
        AES_init_ctx_iv(&this->ctx, this->key, this->iv);
        AES_CBC_decrypt_buffer(&this->ctx, buffer, CHUNK);
        
        for(int idx=0; idx < CHUNK; idx++)
        {
            tChArr[idx + (out * CHUNK)] = buffer[idx];
        }
    }
    result+=tChArr; // maybe memset for security reasons; now always 64 chunks;
    return result;
}


vector<string> Controller::getVaultRows(string data)
{
    vector<string> result;
    size_t delIndex;
    
    while((delIndex = data.find(DELIMITER)))
    {
        int len = (int)string(DELIMITER).length();
        
        if(delIndex+len > data.length())
        {
            break;
        }
        result.push_back(data.substr(0,delIndex));
        data = data.substr(delIndex+len,data.length());
    }
    
    return result;
}

void Controller::mutateArray(uint8_t *src, uint8_t *dest, int src_size, int dest_size, int dest_start_pos)
{
    for (int i=0; i<src_size; i++)
    {
        dest[dest_start_pos+i] = src[i];
    }
}

string Controller::generatePassword(int len)
{
    srand((uint32_t)time(0));
    string password;
    
    for(int i=0; i< len; i++)
    {
        password += this->characters[rand() % sizeof(characters)-1];
    }
    
    return password;
}
