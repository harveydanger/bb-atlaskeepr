//
//  fs_writer.hpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 10/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#ifndef fs_writer_hpp
#define fs_writer_hpp

#include <stdio.h>
#include <string>

#include "macro.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

// vault access
#include <iostream>
#include <fstream>
#include <iterator>

#include <vector>

using namespace std;

class FSWriter
{
private:
    
    int readDirContents(string dir, vector<string> &files);

public:
    FSWriter();

    int createDefaultFolder(string _abs_path);

    // Vault Operations
    int createVault(string _abs_path);
    int removeVault(string _abs_path);
    void listAllVaults(string _abs_path);
    
    // Binary
    void writeVault_uint(uint8_t* _data, int data_len, string _abs_path);
    vector<uint8_t> readVault_uint(string _abs_path);
    
    // fs operations
    bool dirExists(string _abs_path);
    bool fileExists(string _abs_path);
    
};
#endif /* fs_writer_hpp */
