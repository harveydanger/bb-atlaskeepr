//
//  vault.cpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 10/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#include "vault.hpp"

Vault::Vault(string _name)
{
    this->setVaultPath(_name);
}

vector<string> Vault::getAllNodes()
{
    vector<string> n;
    
    for(auto&& node: this->nodes)
    {
        string temp = "name: " + node.name + " value: " + node.value;
        n.push_back(temp);
    }
    
    return n;
}

string Vault::getAllNodesPlain()
{
    string n = "";
    for(auto&& node: this->nodes)
    {
        n+= "name: " + node.name + " value: " + node.value + DELIMITER;
    }
    return n;
}

void Vault::printAllNodes()
{
    for(auto&& node: this->nodes)
    {
        string temp = "name: " + node.name + " value: " + node.value;
        if(temp != ATLAS_SERVICE_ROW)
            cout << temp << endl;
    }
}

void Vault::setVaultPath(string _name)
{
    this->name = _name;
}

int Vault::addNode(Node *_node)
{
    for(auto&& node: this->nodes)
    {
        if(node.name == _node->name)
        {
            return ERROR_EXIT_CODE;
        }
    }
    
    this->nodes.push_back(*_node);
    return OK_EXIT_CODE;
}

int Vault::createNode(string _name, string _value)
{
    Node *n = new Node(_name,_value);
    
    return this->addNode(n);
}

string Vault::getNodeValueByName(string _name)
{
    for(auto&& node: this->nodes)
    {
        if(node.name == _name)
        {
            return node.value;
        }
    }
    
    return NOT_FOUND;
}


int Vault::removeNode(string _name)
{
    for(char i = 0; i < this->nodes.size(); i++)
    {
        if(this->nodes[i].name == _name && this->nodes[i].name != ATLAS_PH)
        {
            this->nodes.erase(this->nodes.begin() + i);
            return OK_EXIT_CODE;
        }
    }
    return ERROR_EXIT_CODE;
}

void Vault::generateNode(string _dirty)
{
    vector<string> result;
    const char *str = _dirty.c_str();
    char c = ' ';
    do
    {
        const char *begin = str;
        
        while(*str != c && *str)
            str++;
        
        result.push_back(string(begin, str));
    } while (0 != *str++);
    
    if(result.size() == 4) // 4 - structural argument list
    {
        this->addNode(new Node(result[1], result[3]));
    }
}
