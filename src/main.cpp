//
//  main.cpp
//  atlas_keepr
//
//  Created by Vladimir Sukhov on 10/8/18.
//  Copyright © 2018 Vladimir Sukhov. All rights reserved.
//

#include <iostream>
#include "controller.hpp"
#include "vault.hpp"

#define CBC 1

#include "aes.hpp"
#include <string>


int main(int argc, const char *argv[])
{
    Controller *core = new Controller();
    core->run(argc, argv);
    
    
    return 0;
}
